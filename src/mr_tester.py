#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright 2015-2021 Łukasz KozŁowski
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License

from __future__ import print_function
import argparse
import subprocess
import shlex
import os
import os.path
import sys
import time
import datetime
import inspect
import collections
import types
if sys.version_info[0] == 3:
    from configparser import ConfigParser
    from configparser import Error as ConfigParserError
else:
    from ConfigParser import ConfigParser
    from ConfigParser import Error as ConfigParserError



class TerminalColor(object):
    "Helper class used to print colorful strings on terminal."
    if os.name == "posix":
        RESET = "\033[0m"
        GREEN = "\033[32m"
        RED = "\033[31m"
        BLUE = "\033[34m"
        PURPLE = "\033[35m"
        YELLOW = "\033[93m"
    else:
        RESET = ""
        GREEN = ""
        RED = ""
        BLUE = ""
        PURPLE = ""
        YELLOW = ""
    @classmethod
    def _colored_text(cls, text, color):
        return color + text + cls.RESET
    @classmethod
    def green(cls, text):
        "Return green text."
        return cls._colored_text(text, cls.GREEN)
    @classmethod
    def red(cls, text):
        "Return red text."
        return cls._colored_text(text, cls.RED)
    @classmethod
    def blue(cls, text):
        "Return blue text."
        return cls._colored_text(text, cls.BLUE)
    @classmethod
    def purple(cls, text):
        "Return purple text."
        return cls._colored_text(text, cls.PURPLE)
    @classmethod
    def yellow(cls, text):
        "Return yellow text."
        return cls._colored_text(text, cls.YELLOW)

class SingleTestRunner(object):
    "Helper class used to run single test."
    def __init__(self, test_class, global_namespace):
        self.test_class = test_class
        self.global_namespace = global_namespace
        self.test_passed = False
        self.failure_reason = None
        self.begin_time_of_test_run = None
        self.end_time_of_test_run = None
    def get_test_name(self):
        "Return name of running test."
        return self.test_class.__name__
    def run(self, verbose):
        "Run test code and report if it pass."
        print("{0} : {1}".format(TerminalColor.purple("Test"), self.test_class.__name__))
        self.begin_time_of_test_run = datetime.datetime.now()
        test_object = self.test_class()
        variables_expected_at_run_phase = {
            "run_test": self._run_test,
            "test_object": test_object,
            "verbose": verbose,
            "test_class": self.test_class
        }
        self.global_namespace.update(variables_expected_at_run_phase)
        self.test_passed = eval("run_test(test_object, verbose)", self.global_namespace)
        self.end_time_of_test_run = datetime.datetime.now()
        return self.test_passed
    @staticmethod
    def _run_test_setup(test, verbose):
        "Run test setup method."
        if hasattr(test, "setup"):
            if verbose:
                print("Running test setup.")
            test.setup()
        else:
            if verbose:
                print("No test setup.")
    @staticmethod
    def _run_test_cleanup(test, verbose):
        "Run test cleanup method."
        if hasattr(test, "cleanup"):
            if verbose:
                print("Running test cleanup.")
            test.cleanup()
        else:
            if verbose:
                print("No test cleanup.")
    def _run_test_steps(self, test, verbose):
        "Run all methods of test objects that are test steps (in proper order)."
        test_steps = [getattr(test, method_name)
                      for method_name in test.__ordered_public_member_names__
                      if self._is_method_a_test_step(test, method_name)]
        if test_steps:
            for test_step in test_steps:
                if verbose:
                    print("Running step: {0}.".format(self._get_test_step_name(test_step)))
                try:
                    test_step()
                except Exception as ex:
                    print("Test step {0} failed.".format(self._get_test_step_name(test_step)))
                    print("Failure: {0}".format(ex))
                    self.failure_reason = str(ex)
                    return False
        else:
            print("Nothing to be done")
        return True
    def _run_test(self, test_object, verbose):
        "Run all stages of test code"
        self._run_test_setup(test_object, verbose)
        test_passed = self._run_test_steps(test_object, verbose)
        self._run_test_cleanup(test_object, verbose)
        return test_passed
    @staticmethod
    def _is_method_a_test_step(test, method_name):
        "Check if given method is a test step (check by name or method attribute)."
        if (inspect.ismethod(getattr(test, method_name))
                and (method_name.startswith("step")
                     or (hasattr(getattr(test, method_name), "test_step")
                         and getattr(test, method_name).test_step is True))):
            return True
        return False
    @staticmethod
    def _get_test_step_name(test_step):
        "Extract test step name from method name."
        if test_step.__name__.startswith("step_"):
            return test_step.__name__[5:]
        if test_step.__name__.startswith("step"):
            return test_step.__name__[4:]
        return test_step.__name__
    def get_total_run_time(self):
        "Return total run time of all tests in ms."
        return int((self.end_time_of_test_run - self.begin_time_of_test_run).total_seconds() * 1000)

class TestCase(object):
    "This is container for tests stored in single directory (and probably logically connected)."
    def __init__(self, test_case_dir):
        self.name = test_case_dir
        self.test_case_dir = test_case_dir
        self.test_definitions = []
        self.executed_tests = []
        self.global_namespace = {}
    def add_test(self, test_definition):
        "Add test definition class to this test case."
        self.test_definitions.append(test_definition)
    def run_tests(self, verbose):
        "Run all tests defined in this test case and print results."
        print("{0}: {1}".format(TerminalColor.yellow("Test case"), self.name))
        if self.test_definitions:
            for test_class in self.test_definitions:
                test_runner = SingleTestRunner(test_class, self.global_namespace)
                test_runner.run(verbose)
                self.executed_tests.append(test_runner)
                if test_runner.test_passed:
                    print(TerminalColor.green("Test PASSED ({0} ms)")
                          .format(test_runner.get_total_run_time()))
                else:
                    print(TerminalColor.red("Test FAILED ({0} ms)")
                          .format(test_runner.get_total_run_time()))
        else:
            print("Nothing to be done.")
    def get_short_name(self):
        "Return short name of this test case (name of test case directory)."
        return self.name.split(os.path.sep)[-1]
    def print_test_names(self):
        "Print names of all test from this test case."
        print("{0}: {1}".format(TerminalColor.yellow("Test case"), self.name))
        for test_definition in self.test_definitions:
            self._print_test_name(test_definition)
    @staticmethod
    def _print_test_name(test):
        """Print test name (which is test definition class name)
        with description extracted from docstring if present."""
        if test.__doc__ is not None:
            print("{0}: {1} ({2})".format(TerminalColor.purple("Test"),
                                          test.__name__, inspect.cleandoc(test.__doc__)))
        else:
            print("{0}: {1}".format(TerminalColor.purple("Test"), test.__name__))
    def get_tests_count(self):
        "Return number of executed tests."
        return len(self.executed_tests)
    def how_many_tests_passed(self):
        "Return total number of passed tests."
        return [test.test_passed for test in self.executed_tests].count(True)
    def how_many_tests_failed(self):
        "Return total number of failed tests."
        return [test.test_passed for test in self.executed_tests].count(False)
    def get_total_run_time(self):
        "Return total run time of all tests in this test case (in miliseconds)."
        return sum((test.get_total_run_time() for test in self.executed_tests))
    @staticmethod
    def _create_single_test_run_summary(test):
        "Create XML test run summary for single test."
        if test.test_passed:
            return '<testcase name="{0}"/>'.format(test.get_test_name())
        return ('<testcase name="{0}"><failure>{1}</failure></testcase>'
                .format(test.get_test_name(), test.failure_reason))
    def create_xml_test_run_summary(self):
        "Create test run summary in JUnit XML format."
        result_lines = (['<testsuite name="{0}" tests="{1}" failures="{2}" time="{3}">'
                            .format(self.get_short_name(), self.get_tests_count(),
                                    self.how_many_tests_failed(), self.get_total_run_time())] +
                        [self._create_single_test_run_summary(executed_test)
                            for executed_test in self.executed_tests] +
                        ['</testsuite>'])
        return "\n".join(result_lines)

class TestCaseRepository(object):
    "Object of this class stores test cases with test definitions."
    def __init__(self):
        self.test_cases = []
        self.current_test_case = None
    def create_new_test_case(self, test_case_dir, globals_dict):
        "Create new test case in repository and start using it as current test case."
        self.current_test_case = TestCase(test_case_dir)
        self.current_test_case.global_namespace.update(globals_dict)
        self.test_cases.append(self.current_test_case)
    def add_test_class(self, test_class):
        "Add test definition class to current test case."
        self.current_test_case.add_test(test_class)
    def get_number_of_test_cases(self):
        "Return total number of test cases registered in repository."
        return len(self.test_cases)
    def get_number_of_tests(self):
        "Return total number of tests defined in all test cases registered in repository."
        return sum([len(test_case.test_definitions) for test_case in self.test_cases])
    def get_total_run_time(self):
        """Return total run time of all tests defined in all test cases
        registered in repository (in miliseconds)."""
        return sum([test_case.get_total_run_time() for test_case in self.test_cases])
    def how_many_tests_passed(self):
        "Return number of all tests that pass from all test cases registered in repository."
        return sum([test_case.how_many_tests_passed() for test_case in self.test_cases])
    def how_many_tests_failed(self):
        "Return number of all tests that fail from all test cases registered in repository."
        return sum([test_case.how_many_tests_failed() for test_case in self.test_cases])

class TestDefinitionMetaClass(type):
    """Metaclass for test definition classes.
    Every test class created with this metaclass will be registered in test repository."""
    test_case_repository = None
    def __prepare__(cls, bases, **kwrds):
        """Class created by this metaclass will use OrderedDict instead of dict
        to remember order of method definitions because it is important to run
        test steps in order of definition and not in alphabetical order."""
        return collections.OrderedDict()
    def __new__(cls, name, bases, namespace, **kwrds):
        test_class_dict = dict(namespace)
        ordered_public_member_names = cls._get_ordered_public_member_names(bases, namespace)
        test_class_dict["__ordered_public_member_names__"] = ordered_public_member_names
        test_definition_class = type.__new__(cls, name, bases, test_class_dict)
        if cls._should_be_registered_to_run_as_test(test_definition_class):
            cls._register_test_definition_class(test_definition_class)
        return test_definition_class
    @staticmethod
    def _extract_ordered_public_member_names(base_class):
        "If base class has defined __ordered_public_member_names__ attribute extract its values."
        if hasattr(base_class, "__ordered_public_member_names__"):
            return [member for member in getattr(base_class, "__ordered_public_member_names__")]
        return []
    @staticmethod
    def _get_public_member_names_from_namespace(namespace):
        """Get public members defined in current class.
        Member names are returned in order of definition."""
        return [key for key in namespace.keys() if not key.startswith('_')]
    @staticmethod
    def _get_ordered_public_member_names(bases, namespace):
        """Return ordered names of public member defined in current class
        and inherited from all base classes."""
        ordered_public_member_names = []
        for base_class in bases:
            base_members = TestDefinitionMetaClass._extract_ordered_public_member_names(base_class)
            ordered_public_member_names.extend(base_members)
        members = TestDefinitionMetaClass._get_public_member_names_from_namespace(namespace)
        ordered_public_member_names.extend(members)
        return ordered_public_member_names
    @staticmethod
    def _should_be_registered_to_run_as_test(test_definition_class):
        """Classes created by this metaclass should be registered to run as tests
        but if class have attribute __test__ set to False it won't be registered
        and can be used e.g. as base class of test definitions."""
        if "__test__" in test_definition_class.__dict__:
            return test_definition_class.__test__
        return True
    @classmethod
    def _register_test_definition_class(cls, test_definition_class):
        "Register new class in test repository for future processing."
        cls.test_case_repository.add_test_class(test_definition_class)

if sys.version_info[0] == 3:
    # definition of Test base class and @test decorator for Python 3
    # have to use 'exec' function because Python2 can't parse this syntax
    exec('class Test(metaclass=TestDefinitionMetaClass):\n'
         '  "This class should be base class for all test definitions."\n'
         '  __test__ = False')
    def test_decorator(test_function):
        "Decorator used to create tests based on single function."
        def test_step(self):
            test_function()
        class_body = {"step_test_body" : test_step, "__doc__" : test_function.__doc__}
        types.new_class(test_function.__name__, (Test,), {}, lambda ns: ns.update(class_body))
elif sys.version_info[0] == 2 and sys.version_info[1] == 7:
    # definition of Test base class and @test decorator for Python/Jython 2.7
    class Test:
        "This class should be base class for all test definitions."
        __metaclass__ = TestDefinitionMetaClass
        __test__ = False
    def test_decorator(test_function):
        "Decorator used to create tests based on single function."
        def test_step(self):
            test_function()
        class_body = {"step_test_body" : test_step}
        class_name = test_function.__name__
        globals()[class_name] = type(class_name, (Test,), class_body)
else:
    raise Exception("Must be using Python 2.7+ or Python 3")

def step_decorator(test_step_function):
    "Decorator used to mark class methods as test steps."
    test_step_function.test_step = True
    return test_step_function

class TestFailed(Exception):
    "This exception should be raised to mark test as failed."
    def __init__(self, failure_description=None):
        super(TestFailed, self).__init__()
        self.failure_description = failure_description
    def __str__(self):
        return "" if self.failure_description is None else self.failure_description

def test_failed():
    "Function used in test definitions to mark test as failed."
    raise TestFailed()

class Ensure(object):
    "Class used to write conditions in test definitions."
    def __init__(self, expression):
        self.expression = expression
    def equal(self, other_expression):
        "Check if given expression is equal to the one passed to constructor."
        if self.expression != other_expression:
            raise TestFailed("Expression '{0}' should equal '{1}'"
                             .format(self.expression, other_expression))
    def not_equal(self, other_expression):
        "Check if given expression isn't equal to the one passed to constructor."
        if self.expression == other_expression:
            raise TestFailed("Expression '{0}' should not equal '{1}'"
                             .format(self.expression, other_expression))
    def is_empty(self):
        "Check if given expression is empty."
        if self.expression:
            raise TestFailed("Expression '{0}' should be empty.".format(self.expression))
    def not_empty(self):
        "Check if given expression is not empty."
        if not self.expression:
            raise TestFailed("Expression '{0}' shouldn't be empty.".format(self.expression))
    def is_true(self):
        "Check if given expression is true."
        if not self.expression:
            raise TestFailed("Expression '{0}' should be True.".format(self.expression))
    def is_false(self):
        "Check if given expression is false."
        if self.expression:
            raise TestFailed("Expression '{0}' should be False.".format(self.expression))

def ensure(expression):
    "Helper/stylistic function - create Ensure object without use of capital letter."
    return Ensure(expression)

# Auxiliary functions that can be used in test definitions
# This functions goes to "utils" namespace available in test definition files
def wait_for_line_containing_text_in_file(text, filepath):
    """Open given file at the end and read it's content like tail -f
    until line with given text appears."""
    file_to_watch = open(filepath)
    # go to the end of file
    file_to_watch.seek(0, 2)
    while True:
        current_line = file_to_watch.readline()
        if not current_line:
            time.sleep(0.1)
            continue
        if text in current_line:
            return current_line

class CommandResult(object):
    """Objects of this class stores results (stdout/stderr output and return code
    of running OS commands."""
    def __init__(self, subprocess_result):
        self.command_output = self._get_command_output(subprocess_result)
        self.return_code = subprocess_result.returncode
    def __str__(self):
        "Conversion to string should return output from stdout/stderr."
        return self.command_output
    def __eq__(self, other):
        return NotImplemented
    def __ne__(self, other):
        "Equal/not equal tests should compare stdout/stderr output."
        return self.command_output != other
    @staticmethod
    def _get_command_output(subprocess_result):
        """Decode output of from run OS command.
        Read stdout or stderr (if stdout isn't present)."""
        command_output = "\n".join([line.decode("utf-8").strip()
                                    for line in subprocess_result.stdout.readlines()])
        if not command_output:
            # std output is empty - try to read std err
            command_output = "\n".join([line.decode("utf-8").strip()
                                        for line in subprocess_result.stderr.readlines()])
        return command_output

class TestRunner(object):
    "Class responsible for loading test cases and running tests."
    def __init__(self, verbose_mode, test_dirs, global_config):
        self.verbose_mode = verbose_mode
        self.test_dirs = test_dirs
        self.test_case_repository = TestCaseRepository()
        # set propertest_repository to register all test classes
        TestDefinitionMetaClass.test_case_repository = self.test_case_repository
        self.globals_dict = global_config
        self.current_test_case_dir = None
        self._register_mrtester_builtins()
    def _register_mrtester_builtins(self):
        "Register types and functions to use in test definition as part of __builtines__ module."
        __builtins__.Test = Test
        __builtins__.test = test_decorator
        __builtins__.step = step_decorator
        __builtins__.ensure = ensure
        __builtins__.test_failed = test_failed
        __builtins__.run = self._run_os_command
        __builtins__.run_async = self._run_async_os_command
        # this is a namespace available in test definition files
        # this namespace contains some auxiliary functions
        utils_namespace = argparse.Namespace(
            wait_for_line_containing_text_in_file = wait_for_line_containing_text_in_file
        )
        __builtins__.utils = utils_namespace
    def _run_os_command(self, command, fail_on_error=True):
        "Run OS command as subprocess."
        if self.verbose_mode:
            print("\tRunning: {0} ... ".format(command), end="")
        try:
            begin_of_command_execution = datetime.datetime.now()
            result = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE,
                                      stderr=subprocess.PIPE, cwd=self.current_test_case_dir)
            result.wait()
            end_of_command_execution = datetime.datetime.now()
            if self.verbose_mode:
                print(TerminalColor.green("DONE") +
                      " ({0}) ms".format(int((end_of_command_execution -
                                              begin_of_command_execution).total_seconds() * 1000)))
            return CommandResult(result)
        except Exception as ex:
            if self.verbose_mode:
                print(TerminalColor.red("FAILED"))
            print(ex)
            if fail_on_error:
                test_failed()
            return None
    def _run_async_os_command(self, command, fail_on_error=True):
        "Run OS command as subprocess and don't wait for result."
        if self.verbose_mode:
            print("\tRunning async: {0} ... ".format(command), end="")
        try:
            subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE,
                             stderr=subprocess.PIPE, cwd=self.current_test_case_dir)
        except Exception as ex:
            if self.verbose_mode:
                print(TerminalColor.red("FAILED"))
            print(ex)
            if fail_on_error:
                test_failed()
        if self.verbose_mode:
            print(TerminalColor.green("DONE"))
    def set_global(self, global_name, global_value):
        """Set global variable visible to test definition code
        when test definition file is loaded and executed."""
        self.globals_dict[global_name] = global_value
    def _load_test_definition_file(self, test_case_dir):
        """Load and execute test definition file from given directory.
        Execution of test definition file will probably populate test_repository
        with new test classes."""
        try:
            test_definition_file_path = os.path.join(test_case_dir, "test_definition.py")
            test_definition_file = open(test_definition_file_path)
            with test_definition_file:
                test_definition = test_definition_file.read()
                self.test_case_repository.create_new_test_case(test_case_dir, self.globals_dict)
                compiled_test_definition = compile(test_definition,
                                                   test_definition_file_path, "exec")
                exec(compiled_test_definition,
                     self.test_case_repository.current_test_case.global_namespace)
                return True
        except IOError:
            print("Error: Failed to open test definition: '{0}'. File not found."
                  .format(test_definition_file_path))
            return False
    def list_test_names(self):
        "Load test definition files and print test case and test names."
        for current_dir in self.test_dirs:
            self._load_test_definition_file(current_dir)
            self.test_case_repository.current_test_case.print_test_names()
    def run_tests(self):
        "Load test definition files and run all tests."
        for current_dir in self.test_dirs:
            self._load_test_definition_file(current_dir)
        for test_case in self.test_case_repository.test_cases:
            self.current_test_case_dir = test_case.test_case_dir
            test_case.run_tests(self.verbose_mode)
    def print_summary(self):
        "Print summary of all run tests."
        print("\nSummary:")
        print("{0} tests from {1} test cases ran ({2} ms total)."
              .format(self.test_case_repository.get_number_of_tests(),
                      self.test_case_repository.get_number_of_test_cases(),
                      self.test_case_repository.get_total_run_time()))
        passed_tests_count = self.test_case_repository.how_many_tests_passed()
        failed_tests_count = self.test_case_repository.how_many_tests_failed()
        if passed_tests_count == 1:
            print("{0}: 1 test.".format(TerminalColor.green("PASSED")))
        else:
            print("{0}: {1} tests.".format(TerminalColor.green("PASSED"), passed_tests_count))
        if failed_tests_count == 1:
            print("{0}: 1 test.".format(TerminalColor.red("FAILED")))
        elif failed_tests_count > 1:
            print("{0}: {1} tests.".format(TerminalColor.red("FAILED"), failed_tests_count))
    def create_xml_summary(self):
        "Print XML test run summary (in JUnit format) for all testcases."
        with open("results.xml", "w") as result_file:
            tests_count = self.test_case_repository.get_number_of_tests()
            failed_tests_count = self.test_case_repository.how_many_tests_failed()
            total_tests_run_time = self.test_case_repository.get_total_run_time()
            result_file.write('<testsuites tests="{0}" failures="{1}" time="{2}">\n'
                                .format(tests_count, failed_tests_count, total_tests_run_time))
            for test_case in self.test_case_repository.test_cases:
                result_file.write(test_case.create_xml_test_run_summary())
                result_file.write('\n')
            result_file.write('</testsuites>\n')
    def get_number_of_failed_tests(self):
        "Return number of failed tests. Only executed tests counts."
        return self.test_case_repository.how_many_tests_failed()

def parse_commandline_arguments():
    "Handle commandline arguments and return parsed options."
    argument_parser = argparse.ArgumentParser(description="Run functional tests.")
    argument_parser.add_argument("test_directories", type=str, nargs="*",
                                 help="Test directories to process.")
    argument_parser.add_argument("--config", type=str, dest="config_path",
                                 help="Configuration file.")
    argument_parser.add_argument("--list", dest="list_test_names", action="store_true",
                                 help="List test names and descriptions.")
    argument_parser.add_argument("--create_xml_summary", dest="create_xml_summary",
                                 action="store_true",
                                 help="Create XML files with test run summary (JUnit format).")
    argument_parser.add_argument("--verbose", dest="verbose", action="store_true")
    argument_parser.set_defaults(verbose=False)
    argument_parser.set_defaults(list_test_names=False)
    argument_parser.set_defaults(create_xml_summary=False)
    return argument_parser.parse_args()

def get_test_dir_paths(test_directories):
    "Create list of filesystem paths to test case directories."
    root_dir = os.getcwd()
    if test_directories:
        # process only tests passed as commandline arguments
        return [os.path.join(root_dir, o) for o in test_directories]
    # process all tests from tests root
    return [os.path.join(root_dir, o)
            for o in os.listdir(root_dir)
            if os.path.isdir(os.path.join(root_dir, o)) and not o.startswith(".")]

def load_config(config_path):
    """Read config file (if present) and return dict based on [Global] section.
    If config_path is None return empty dict. In case of an error return None."""
    if config_path is not None:
        # read options from configuration file
        config = ConfigParser()
        # handle capital letters in option names as they are in config file
        config.optionxform = str
        try:
            if sys.version_info[0] == 3:
                config.read_file(open(config_path))
            else:
                config.readfp(open(config_path))
        except EnvironmentError as err:
            print("Error: unable to load config file: {} ({})"
                  .format(config_path, os.strerror(err.errno)))
            return None
        try:
            return dict(config.items("Global"))
        except ConfigParserError as err:
            print("Error: broken config file structure ({})".format(err))
            return None
    return {}

def main():
    "Mr Tester main function."
    arguments = parse_commandline_arguments()
    test_dirs = get_test_dir_paths(arguments.test_directories)
    global_config = load_config(arguments.config_path)
    if global_config is None:
        return -1
    test_runner = TestRunner(arguments.verbose, test_dirs, global_config)
    if arguments.list_test_names:
        test_runner.list_test_names()
    else:
        test_runner.run_tests()
        test_runner.print_summary()
        if arguments.create_xml_summary:
            test_runner.create_xml_summary()
    return test_runner.get_number_of_failed_tests()



if __name__ == "__main__":
    sys.exit(main())
