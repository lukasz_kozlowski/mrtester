class SimplePrintTest(Test):
    "Simple test with single step."
    def step1(self):
        print("First and only test step.")

class MultistepTest(Test):
    "Simple test with two steps."
    def step_first(self):
        print("First test step.")
    def step_another(self):
        print("Second test step.")
