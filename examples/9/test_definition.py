class FailedTestWithEnsureExample(Test):
    @step
    def check_important_value(self):
        value = 4
        ensure(value).equal(10)

