class SimpleTestWithSetupAndCleanup(Test):
    "Demonstration of test setup and cleanup."
    def setup(self):
        print("Test setup.")
    def step_first_test_step(self):
        print("First test step.")
    def step_second_test_step(self):
        print("Second test step.")
    def cleanup(self):
        print("Test cleanup.")

