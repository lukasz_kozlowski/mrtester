class RunSystemCommandTest(Test):
    "This test will run OS command 'uname -a'"
    def step_run_uname_command(self):
        uname_result = run("uname -a")
        print("Uname result: {0}".format(uname_result))
    def step_run_pwd_command(self):
        pwd_result = run("pwd")
        print("pwd result: {0}".format(pwd_result))
    def step_run_unknown_command(self):
        wrong_result = run("there_is_no_such_command", fail_on_error=False)

class FailOnLinuxTest(Test):
    "This test will check OS (with help of uname command) and fail on Linux."
    def step_ensure_not_linux(self):
        os_name = run("uname")
        ensure(os_name).not_equal("Linux")

class FailOnNonlinuxTest(Test):
    "This step will fail on any OS other than Linux."
    def step_ensure_linux(self):
        os_name = run("uname")
        ensure(os_name).equal("Linux")

