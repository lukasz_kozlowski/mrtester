class TestBaseClass(Test):
    """This class is based on Mr Tester Test class but set special
    attribute __test__ to False so it won't be registered nor run as test.
    It can be used as base class for other test definitions."""
    __test__ = False
    def setup(self):
        print("Setup")
    @step
    def first_step(self):
        print("First step")
    @step
    def second_step(self):
        print("Second step")
    def cleanup(self):
        print("Cleanup")

class TestWithBaseClass1(TestBaseClass):
    @step
    def third_step(self):
        print("Third step from TestWithBaseClass1")

class TestWithBaseClass2(TestBaseClass):
    @step
    def third_step(self):
        print("Third step from TestWithBaseClass2")
